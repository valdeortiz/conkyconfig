import requests
from bs4 import BeautifulSoup

# direccion de meteorologia e hidrologia
def dmh():

    try:
        response = requests.get("https://www.meteorologia.gov.py/", timeout=10)
    except :
        print("No se pudo conectar a meteorologia")
        return 0,0

    try:
        soup = BeautifulSoup(response.content,"html.parser") #bs parsear el documento y response.content es para acceder a la respuesta del servidor,, y 2do para: el tipo de archivo que queremos parsear 
        forecast_today = soup.find(class_="forecast-today").get_text()
        forecast_today = forecast_today.replace("\n\n"," ").replace("\n","").strip().split()
        presion = " ".join(forecast_today[:3])
        sensacion_termica = " ".join(forecast_today[3:6])
        humedad = " ".join(forecast_today[6:9])
        viento = " ".join(forecast_today[9:12])
        result = f"{presion} \n {sensacion_termica} \n {humedad} \n {viento}"
        #falta temperatura
        #verificar si es que retorna todos los datos
    except Exception as e:
        #print(e)
        print("Error al leer el forecast_today")
        return 0,0	

    try:
        fecha = soup.find(class_="updated").get_text()
        fecha = fecha.replace("\n\n"," ").replace("\n","").strip()
        #verificar si es que retorna todos los datos
    except Exception as e:
        #print(e)
        print("Error al leer la fecha")
        return result, 0

    return result , fecha


if __name__ == "__main__":
    result, fecha = dmh()
   
    print("\n*** Direccion de meteorologia ***")
    print(f"{fecha} \n{result} \n  ")
